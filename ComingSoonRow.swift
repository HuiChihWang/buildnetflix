//
//  ComingSoonRow.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/19.
//

import SwiftUI
import KingfisherSwiftUI
import AVKit

struct ComingSoonRow: View {
    let screen = UIScreen.main.bounds
    
    var movie: Movie
    @Binding var movieDetailToShow: Movie?
    
    var body: some View {
        VStack {
            VideoView(url: exampleVideoURL)
                .frame(height: 200)
            
            VStack {
                InfoRow(movie: movie)
                    .frame(width: screen.width, height:80)
                
                
                VStack(alignment: .leading) {
                    Text(movie.name)
                        .foregroundColor(.white)
                        .font(.title2)
                        .bold()
                        .padding(.bottom, 3)
                    
                    Text(movie.description)
                        .foregroundColor(.gray)
                        .font(.headline)
                    
                    CatogoriesList(catogories: movie.catogories)
                        .foregroundColor(.white)
                        .padding(.top, 5)
                    
                }
            }
            .padding(.horizontal, 10)
        }
    }
}

struct InfoRow: View {
    var movie: Movie
    @State private var isRemindMe: Bool = false
    
    var body: some View {
        GeometryReader { geometry in
            HStack {
                KFImage(movie.thumbnailURL)
                    .resizable()
                    .scaledToFill()
                    .frame(width: geometry.size.width * 0.35, height: geometry.size.height)
                    .clipped()
                
                Spacer()
                
                SmallVerticalButton(text: "Remind Me", isOffImage: "bell", isOnImage: "checkmark", isOn: self.isRemindMe) {
                    self.isRemindMe.toggle()
                }
                .foregroundColor(isRemindMe ? .red : .white)
                .padding(.trailing, 10)
                
                SmallVerticalButton(text: "Info", isOffImage: "info.circle", isOnImage: "info.circle", isOn: true) {
                    
                }
                .foregroundColor(.white)
                .padding(.trailing, 20)
            }
            .font(.system(size: 20))
        }
        

    }
}

struct ComingSoonRow_Previews: PreviewProvider {
    static var previews: some View {
        
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            ComingSoonRow(movie: exampleMovie7, movieDetailToShow: .constant(nil))
        }
    }
}
