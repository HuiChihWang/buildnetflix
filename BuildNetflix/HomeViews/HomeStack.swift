//
//  HomeStack.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/17.
//

import SwiftUI

struct HomeStack: View {
    var homeVM: HomeVM
    @Binding var topRowSelection: HomeTopRow
    @Binding var homeGenre: HomeGenre
    @Binding var movieDetailToShow: Movie?
    
    var body: some View {
        ForEach(homeVM.allCategories, id: \.self) { catogory in
            VStack {
                HStack {
                    Text(catogory)
                        .font(.title3)
                        .bold()
                    
                    Spacer()
                }
                
                ScrollView(.horizontal, showsIndicators: false) {
                    LazyHStack {
                        ForEach(homeVM.getMovie(for: catogory, andHomeRow: topRowSelection, andGenre: homeGenre)) { movie in
                            StandardHomeMovie(movie: movie)
                                .frame(width: 100, height: 200)
                                .padding(.horizontal, 20)
                                .onTapGesture {
                                    movieDetailToShow = movie
                                }
                        }
                    }
                }
            }
        }
    }
}

struct HomeStack_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            ScrollView(.vertical, showsIndicators: false, content: {
                HomeStack(homeVM: HomeVM(), topRowSelection: .constant(.home), homeGenre: .constant(.AllGenres), movieDetailToShow: .constant(nil))
            })
        }
        .foregroundColor(.white)
    }
}
