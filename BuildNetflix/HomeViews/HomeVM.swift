//
//  HomeVM.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/11.
//

import Foundation

class HomeVM: ObservableObject {
    
    // String : Categories
    @Published var movies: [String: [Movie]] = [:]
    
    public var allCategories: [String] {
        
        return movies.keys.map { String($0) }
    }
    
    public var allGenres: [HomeGenre] = [.AllGenres, .Action, .Comedy, .Horror, .Triller]
    
    
    public func getMovie(for category: String, andHomeRow homeRow: HomeTopRow, andGenre genre: HomeGenre) -> [Movie] {
        
        switch homeRow {
        case .home:
            return movies[category] ?? []
        case .movies:
            return (movies[category] ?? []).filter{($0.movieType == .movie) && ($0.movieGenre == genre)}
        case .tvShows:
            return (movies[category] ?? []).filter{($0.movieType == .tvShow) && ($0.movieGenre == genre)}
        case .myList:
            return movies[category] ?? [] // TODO
        
        }
    }
    
    init() {
        setupMovies()
        
    }
    
    func setupMovies() {
        movies["Trending New"] = exampleMovies.shuffled()
        movies["Trailer"] = exampleMovies.shuffled()
        movies["New Release"] = exampleMovies.shuffled()
        movies["Sci Fi"] = exampleMovies.shuffled()
        movies["Comics"] = exampleMovies.shuffled()
    }
    
    
    
}
