//
//  HomeView.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/11.
//

import SwiftUI

struct HomeView: View {
    static let screenSize = UIScreen.main.bounds
    
    @ObservedObject var vm = HomeVM()
    
    @State private var movieDetailToShow: Movie?
    
    @State private var topRowSelection: HomeTopRow = .home
    @State private var homeGenre: HomeGenre = .AllGenres
    
    @State private var showGenreSelection: Bool = false
    @State private var showTopRowSelection: Bool = false
    
    @ViewBuilder
    private func mainView() -> some View {
        ScrollView(.vertical, showsIndicators: false) {
            LazyVStack {
                TopRowButtons(
                    topRowSelection: $topRowSelection,
                    homeGenre: $homeGenre,
                    showGenreSelection: $showGenreSelection,
                    showTopRowSelection: $showTopRowSelection
                )
                    .padding(.leading, 10)
                    .padding(.trailing, 30)
                    .foregroundColor(.white)
                
                TopMoviePreview(movie: exampleMovie5)
                    .frame(width: HomeView.screenSize.width)
                    .padding(.top, -100)
                    .zIndex(-1)
                
                MoviePreviewRow(previewMovies: exampleMovies)
                    .frame(height: 150)
                
                HomeStack(homeVM: vm, topRowSelection: $topRowSelection, homeGenre: $homeGenre, movieDetailToShow: $movieDetailToShow)
            }
        }
    }
    
    @ViewBuilder
    private func genreSelectorView() -> some View {
        Group {
            Color.black
                .opacity(0.9)
            
            VStack(spacing: 40) {
                ScrollView(.vertical) {
                    ForEach(vm.allGenres, id: \.self) { genre in
                        Button(action: {
                            homeGenre = genre
                            showGenreSelection = false
                        }, label: {
                            Text(genre.rawValue)
                                .font(.system(
                                    size: genre == homeGenre ? 30 : 20,
                                    weight: genre == homeGenre ? .bold : .regular
                                ))
                                .foregroundColor(genre == homeGenre ? .white : .gray)
                                
                        })
                        .padding(.bottom, 40)
                    }
                }
                
                Spacer()

                Button(action: {
                    showGenreSelection = false
                }, label: {
                    Image(systemName: "x.circle.fill")
                        .foregroundColor(.white)
                        .font(.system(size: 40))
                        .scaleEffect(1.1)
                })
                .padding(.bottom, 30)
            }
        }
        .edgesIgnoringSafeArea(.all)
    }
    
    @ViewBuilder
    private func topRowSelectorView() -> some View {
        Group {
            Color.black
                .opacity(0.9)
            
            VStack(spacing: 40) {
                Spacer()
                
                ForEach(HomeTopRow.allCases, id: \.self) { topRow in
                    Button(action: {
                        topRowSelection = topRow
                        showTopRowSelection = false
                    }, label: {
                        Text(topRow.rawValue)
                            .font(.system(
                                size: topRow == topRowSelection ? 30 : 20,
                                weight: topRow == topRowSelection ? .bold : .regular
                            ))
                            .foregroundColor(topRow == topRowSelection ? .white : .gray)
                            
                    })
                }
                
                Spacer()

                Button(action: {
                    showTopRowSelection = false
                }, label: {
                    Image(systemName: "x.circle.fill")
                        .foregroundColor(.white)
                        .font(.system(size: 40))
                        .scaleEffect(1.1)
                })
                .padding(.bottom, 30)
            }
        }
        .edgesIgnoringSafeArea(.all)
    }
    
    var body: some View {

        ZStack {
            Color.black
                .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
            
            // only render things on Screen
            mainView()
            
            if let movieDetailToShow = self.movieDetailToShow {
                MovieDetailView(movie: movieDetailToShow, movieDetailToShow: self.$movieDetailToShow)
                    .animation(.easeIn)
                    .transition(.slide)
            }
            
            if showTopRowSelection {
                topRowSelectorView()
            }
            
            if showGenreSelection {
                genreSelectorView()
            }
        }
        .foregroundColor(.white)
    }
}

enum HomeTopRow: String, CaseIterable {
    case home = "Home"
    case tvShows = "TV Shows"
    case movies = "Movies"
    case myList = "My List"
}

enum HomeGenre: String {
    case AllGenres
    case Action
    case Comedy
    case Horror
    case Triller
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
