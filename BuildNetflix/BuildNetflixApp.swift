//
//  BuildNetflixApp.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/11.
//

import SwiftUI

@main
struct BuildNetflixApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
