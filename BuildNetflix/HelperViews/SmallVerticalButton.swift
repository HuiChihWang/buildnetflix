//
//  SmallVerticalButton.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/12.
//

import SwiftUI

struct SmallVerticalButton: View {
    var text: String
    var isOffImage: String
    var isOnImage: String
    
    var imageName: String {
        return isOn ? isOnImage : isOffImage
    }
    
    var isOn: Bool
    
    var action: () -> Void
    
    var body: some View {
        Button(action: action, label: {
            VStack {
                Image(systemName: imageName)
                    .padding(.bottom, 2)
                
                Text(text)
                    .font(.system(size: 14))
                    .bold()
            }
        })
    }
}

struct SmallVerticalButton_Previews: PreviewProvider {
    
    var isChange: Bool = false
    
    static var previews: some View {
        ZStack {
            Color.black.ignoresSafeArea(.all)
            
            VStack {
                SmallVerticalButton(text: "My List", isOffImage: "checkmark", isOnImage: "plus", isOn: true) {
                    print("Tap Button")
                }
                .padding(.vertical, 100)
                
                SmallVerticalButton(text: "My List", isOffImage: "checkmark", isOnImage: "plus", isOn: false) {
                    print("Tap Button")
                }
            }

        }
    }
}
