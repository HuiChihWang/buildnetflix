//
//  TopMoviePreview.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/12.
//

import SwiftUI
import KingfisherSwiftUI

struct TopMoviePreview: View {
    var movie: Movie
    
    @ViewBuilder
    private func buidMainImage() -> some View {
        KFImage(movie.thumbnailURL)
            .resizable()
            .scaledToFill()
            .clipped()
    }
        
    @ViewBuilder
    private func buildButtons() -> some View {
        HStack() {
            Spacer()
            SmallVerticalButton(text: "My List", isOffImage: "plus", isOnImage: "checkmark", isOn: true) {
                //
            }
            
            Spacer()
            
            RectangleButton(text: "Play", imageName: "play.fill", backgroundColor: .white, forgroundColor: .black) {
                
            }
            .frame(width: 120)
            
            Spacer()
            
            SmallVerticalButton(text: "Info", isOffImage: "info.circle", isOnImage: "info.circle", isOn: true) {
                //
            }
            Spacer()
        }
    }
    
    var body: some View {
        ZStack {
            buidMainImage()
            
            VStack {
                
                Spacer()
                
                CatogoriesList(catogories: movie.catogories)
                    .padding(.bottom, 5)

                buildButtons()
                    .padding(.bottom, 5)
            }
            .background(LinearGradient.blackOpacityGradient.padding(.top, 250))
        }
        .foregroundColor(.white)
    }
}


struct TopMoviePreview_Previews: PreviewProvider {
    static var previews: some View {
        TopMoviePreview(movie: exampleMovie1)
    }
}
