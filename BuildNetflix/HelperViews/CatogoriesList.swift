//
//  CatogoriesList.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/19.
//

import SwiftUI

struct CatogoriesList: View {
    var catogories: [String]
    
    private func isCategoryLast(_ cat: String) -> Bool {
        let catCount = catogories.count
        
        if let index = catogories.firstIndex(of: cat), index != catCount - 1 {
            return false
        }
        
        return true
    }
    
    var body: some View {
        HStack {
            ForEach(catogories, id: \.self) { category in
                HStack {
                    Text(category)
                        .font(.footnote)
                    
                    if !isCategoryLast(category) {
                        Image(systemName: "circle.fill")
                            .foregroundColor(.blue)
                            .font(.system(size: 3))
                    }
                }
            }
        }
    }
}

struct CatogoriesList_Previews: PreviewProvider {
    static var previews: some View {
        CatogoriesList(catogories: exampleMovie1.catogories)
    }
}
