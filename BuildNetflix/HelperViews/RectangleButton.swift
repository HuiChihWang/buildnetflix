//
//  WhiteButton.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/12.
//

import SwiftUI

struct RectangleButton: View {
    var text: String
    var imageName: String
    var backgroundColor: Color
    var forgroundColor: Color
    var action: () -> Void
    
    var body: some View {
        
        Button(action: action, label: {
            HStack {
                Spacer()
                
                Image(systemName: imageName)
                    .font(.headline)
                
                Text(text)
                    .bold()
                    .font(.system(size: 16))
                
                Spacer()
            }
            .padding(.vertical, 6)
            .foregroundColor(forgroundColor)
            .background(backgroundColor)
            .cornerRadius(3.0)
        })
    }
}

struct WhiteButton_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
            RectangleButton(text: "Play", imageName: "play.fill", backgroundColor: .white, forgroundColor: .black) {
                print("Tap Button")
            }
        }
    }
}
