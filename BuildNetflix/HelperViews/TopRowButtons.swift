//
//  TopRowButtons.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/13.
//

import SwiftUI

struct TopRowButtons: View {
    @Binding var topRowSelection: HomeTopRow
    @Binding var homeGenre: HomeGenre
    
    @Binding var showGenreSelection: Bool
    @Binding var showTopRowSelection: Bool
    
    @ViewBuilder
    private func allButtons() -> some View {
        HStack(spacing:20) {
            
            Button(action: {
                topRowSelection = .movies
            }, label: {
                Text("Movie")
            })
            
            Spacer()
            
            Button(action: {
                topRowSelection = .tvShows
            }, label: {
                Text("TV Show")
            })

            Spacer()
            
            Button(action: {
                topRowSelection = .myList
            }, label: {
                Text("My List")
            })
        }
    }
    
    private var netflixButton: some View {
        Button(action: {
            topRowSelection = .home
        }, label: {
            Image("netflix_logo")
                .resizable()
                .scaledToFit()
                .frame(width: HomeView.screenSize.width * 0.15)
        })
    }
    
    var body: some View {
        HStack {
            netflixButton
        
            Spacer()
            
            switch topRowSelection {
            case .home:
                allButtons()
            default:
                HStack(spacing: 20) {
                    
                    Button(action: {
                        showTopRowSelection = true
                    }, label: {
                        HStack {
                            Text(topRowSelection.rawValue)
                                .font(.system(size: 18))
                            Image(systemName: "arrowtriangle.down.fill")
                                .font(.system(size: 16))
                        }
                    })

                    Button(action: {
                        showGenreSelection = true
                    }, label: {
                        HStack {
                            Text("All Genre")
                                .font(.system(size: 14))
                            Image(systemName: "arrowtriangle.down.fill")
                                .font(.system(size: 12))
                        }
                    })
                    
                    Spacer()
                }
            }
            
            
            
//                .padding(.leading, 20)
        }

    }
}

struct TopRowButtons_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            VStack {
                TopRowButtons(
                    topRowSelection: .constant(.movies),
                    homeGenre: .constant(.AllGenres),
                    showGenreSelection: .constant(false),
                    showTopRowSelection: .constant(false)
                )
                    .padding(.leading, 10)
                    .padding(.trailing, 30)
                    .background(Color.black)
                Spacer()
            }
            .foregroundColor(.white)

        }
            
    }
}
