//
//  MovieInfoSubheadline.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/13.
//

import SwiftUI

struct MovieInfoSubheadline: View {
    var movie: Movie
    
    var numberOfSeasonDisplay: String {
        if let season = movie.detail.numberOfSeason {
            return "\(season) season" + (season > 1 ? "s" : "")
        }
        return ""
    }
    
    var body: some View {
        
        HStack(spacing: 15) {
            Image(systemName: "hand.thumbsup.fill")
                .foregroundColor(.white)
            Text(String(movie.detail.year))
            
            Text(movie.detail.rating)
                .modifier(RateModifier())
                
            Text(numberOfSeasonDisplay)
            
            Text(movie.detail.resolution)
                .modifier(ResolutionModifier())
            
        }
        .foregroundColor(.gray)
    }
}

struct RateModifier: ViewModifier {
    func body(content: Content) -> some View {
        ZStack {
            Rectangle()
                .foregroundColor(.gray)
            content
                .font(.system(size: 12, weight: .bold))
                .foregroundColor(.white)
        }
        .frame(width: 50, height: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)

    }
}

struct ResolutionModifier: ViewModifier {
    func body(content: Content) -> some View {
        ZStack {
            RoundedRectangle(cornerRadius: 5)
                .stroke(lineWidth: 2)
            content
                .font(.system(size: 12, weight: .heavy))
        }
        .foregroundColor(.gray)
        .frame(width: 50, height: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
    }
}

struct MovieInfoSubheadline_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
                .ignoresSafeArea(.all)
            VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 10) {
                MovieInfoSubheadline(movie: exampleMovie2)
                MovieInfoSubheadline(movie: exampleMovie3)
                MovieInfoSubheadline(movie: exampleMovie1)
            }
            
        }
        
    }
}
