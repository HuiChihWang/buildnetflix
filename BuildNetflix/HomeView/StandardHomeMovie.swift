//
//  StandardHomeMovie.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/11.
//

import SwiftUI

import SwiftUI
import KingfisherSwiftUI


struct StandardHomeMovie: View {
    var movie: Movie
    
    var body: some View {
        KFImage(movie.thumbnailURL)
            .resizable()
    }
}

struct StandardHomeMovie_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            StandardHomeMovie(movie: generateMovies(1)[0])
        }
    }
}
