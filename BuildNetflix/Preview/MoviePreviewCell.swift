//
//  MoviePreviewCell.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/20.
//

import SwiftUI
import KingfisherSwiftUI

struct MoviePreviewCell: View {
    var movie: Movie
    
    
    var body: some View {
        GeometryReader { geometry in
//            let minLength = min(geometry.size.width, geometry.size.height)
           return  ZStack{
                KFImage(movie.thumbnailURL)
                    .resizable()
                    .scaledToFill()
                    .clipShape(Circle())
                    //TODO: try different colors
                    .overlay(
                    Circle()
                        .stroke(lineWidth: 3)
                        .foregroundColor(randomColor)
                    )
                    .frame(width: geometry.size.height, height: geometry.size.height)
                
                Image(movie.previewImageName)
                    .resizable()
                    .scaledToFit()
                    .frame(width: geometry.size.width, height: geometry.size.height * 0.3)
                    .offset(y:geometry.size.height / 3)
            }
        }
    }
}

struct MoviePreviewCell_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            MoviePreviewCell(movie: exampleMovie6)
                .frame(width: 300, height: 300)
        }
    }
}
