//
//  MoviePreviewRow.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/20.
//

import SwiftUI

struct MoviePreviewRow: View {
    var previewMovies: [Movie] = []
    @State private var movieToShow: Movie?
    
    var body: some View {
        GeometryReader { geometry in
            VStack(alignment: .leading) {
                Text("Previews")
                    .font(.title3)
                    .bold()
                    .foregroundColor(.white)
                
                ScrollView(.horizontal) {
                    LazyHStack {
                        ForEach(previewMovies) { movie in
                            MoviePreviewCell(movie: movie)
                                .frame(width: geometry.size.height * 0.6 , height: geometry.size.height * 0.6)
                                .padding(.horizontal, 8)
                        }
                    }
                }
            }
        }

    }
}

struct MoviePreviewRow_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            MoviePreviewRow(previewMovies: exampleMovies)
                .frame(height: 200)
        }
    }
}
