//
//  SearchResultsGrid.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/19.
//

import SwiftUI

struct SearchResultsGrid: View {
    var movies: [Movie]
    @Binding var movieToShow: Movie?
    
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        VStack {
            HStack {
                Text("Movies & TV")
                    .bold()
                    .font(.title3)
                    .padding(.leading, 12)
                Spacer()
            }
            
            LazyVGrid(columns: columns) {
                ForEach(movies) { movie in
                    StandardHomeMovie(movie: movie)
                        .frame(height: 160)
                        .onTapGesture {
                            movieToShow = movie
                        }
                }
            }
        }
        .foregroundColor(.white)
    }
}

struct SearchResultsGrid_Previews: PreviewProvider {
    static var previews: some View {
        let movies = generateMovies(20)
        
        return ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            
            VStack {
                SearchResultsGrid(movies: movies, movieToShow: .constant(nil))
                Spacer()
            }
            
        }

    }
}
