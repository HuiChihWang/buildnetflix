//
//  PopularMovieView.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/18.
//

import SwiftUI
import KingfisherSwiftUI

struct PoularListView: View {
    var movies: [Movie]
    @Binding var movieToShow: Movie?
    
    var body: some View {
        VStack {
            HStack {
                Text("Popular Searches")
                    .bold()
                    .font(.title3)
                    .padding(.leading, 12)
                Spacer()
            }
            
            LazyVStack {
                ForEach(movies) { movie in
                    PopularMovieView(showMovieDetail: $movieToShow, movie: movie)
                        .frame(height: 75)
                }
            }
        }
    }
}

struct PopularMovieView: View {
    @Binding var showMovieDetail: Movie?
    
    var movie: Movie
    var body: some View {
        
        GeometryReader { geometry in
            HStack {
                KFImage(movie.thumbnailURL)
                    .resizable()
                    .frame(width: geometry.size.width * 0.35, height: geometry.size.height)
                
                
                
                Text(movie.name)
                
                Spacer()
                
                Button(action: {
                    
                }, label: {
                    Image(systemName: "arrowtriangle.right.fill")
                        .padding(.trailing, 20)
                })
            }
            .foregroundColor(.white)
            .onTapGesture {
                showMovieDetail = movie
            }
        }
    }
}

struct PopularListView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            
            PoularListView(movies: exampleMovies, movieToShow: .constant(nil))
            
        }
    }
}


