//
//  SearchView.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/18.
//

import SwiftUI

struct SearchView: View {
    @ObservedObject var vm = SearchVM()
    
    @State var searchQuery: String = ""
    @State var movieToShow: Movie?
    
    var body: some View {
        
        let searTextBinding = Binding {
            return searchQuery
        } set: {
            searchQuery = $0
            vm.updateSearchText(with: $0)
        }
        
        return ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            
            VStack {
                SearchBar(searchQuery: searTextBinding, isLoading: $vm.isLoading)
                    .frame(height: 50)
                    .padding(.top, 10)
                
                ScrollView {
                    if vm.isShowingPopularMovies {
                        PoularListView(movies: vm.popularMovies, movieToShow: $movieToShow)
                            
                    }
                    
                    else {
                        if vm.viewState == .empty {
                            Text("Your search didn't have any result")
                                .bold()
                                .padding(.top, 200)
                        }
                        else if vm.viewState == .ready {
                            SearchResultsGrid(movies: vm.searchResults, movieToShow: $movieToShow)
                                .padding(.horizontal, 10)
                        }
                        else if vm.viewState == .loading {
                            Text("Loading...")
                        }
                    }
                    
                }
            }
            
            // layout movie preview
            if let movieClick = movieToShow {
                MovieDetailView(movie: movieClick, movieDetailToShow: $movieToShow)
            }
        }
        .foregroundColor(.white)
    }
}



struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}
