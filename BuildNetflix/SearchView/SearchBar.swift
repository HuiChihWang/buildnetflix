//
//  SearchBar.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/18.
//

import SwiftUI

struct SearchBar: View {
    @State private var isEditing: Bool = false
    
    @Binding var searchQuery: String
    @Binding var isLoading: Bool
    
    private var isContainText: Bool {
        return !searchQuery.isEmpty
    }
    
    var body: some View {
        GeometryReader { geometry in
            HStack {
                ZStack(alignment: .leading) {
                    Color.graySearchBackground
                        .frame(height: 36)
                        .cornerRadius(10)
                        .animation(.default)
                    
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(.graySearchText)
                            .padding(.leading, 10)
                        
                        //animation not right
                        TextField("Search", text: $searchQuery)
                            .background(Color.graySearchBackground)
                            .cornerRadius(8)
                            .foregroundColor(.white)
                            .onTapGesture {
                                isEditing = true
                            }
                            
                        
                        if isContainText {
                            
                            if isLoading {
                                Button(action: {
                                    searchQuery = ""
                                }, label: {
                                    ActivityIndicator(style: .medium, animate: .constant(true))
                                        .configure {
                                            $0.color = .white
                                        }
                                        .frame(width: 35, height: 35)
                                })
                                
                            }
                            
                            else {
                                Button(action: {
                                    searchQuery = ""
                                }, label: {
                                    Image(systemName: "xmark.circle.fill")
                                        .foregroundColor(.graySearchText)
                                        .frame(width: 35, height: 35)
                                })
                            }
                        }
                    }
                    
                }
                
                if isEditing {
                    Button(action: {
                        // clear text, change back to default view
                        searchQuery = ""
                        isEditing = false
                    }, label: {
                        Text("Cancel")
                            .font(.system(size: 18))
                            .foregroundColor(.white)
                    })
                    .padding(.trailing, 10)
                    .transition(.move(edge: .trailing))
                    .animation(.default)
                }
            }
            .padding(.horizontal, 5)
        }
    }
}

struct SearchBar_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            SearchBar(searchQuery: .constant(""), isLoading: .constant(false))
        }
    }
}
