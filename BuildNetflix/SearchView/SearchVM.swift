//
//  SearchVM.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/18.
//

import Foundation
import SwiftUI

class SearchVM: ObservableObject {
    
    @Published var isLoading: Bool = false
    @Published var viewState: ViewState = .empty
    
    @Published var popularMovies: [Movie] = []
    @Published var searchResults: [Movie] = []
    
    @Published var isShowingPopularMovies: Bool = true
    
    init() {
        generatePopularMovies()
    }
    
    public func updateSearchText(with text: String) {
        setViewState(to: .loading)
        
        if !text.isEmpty {
            isShowingPopularMovies = false
            updateSearchResult(for: text)
        }
        else {
            isShowingPopularMovies = true
        }
        
    }
    
    private func generatePopularMovies() {
        popularMovies = generateMovies(40)
    }
    
    private func updateSearchResult(for text: String) {
        let generateToken = Int.random(in: 0...3)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.searchResults = generateMovies(20)
            self.setViewState(to: generateToken == 0 ?  .empty: .ready)
        }
    }
    
    private func setViewState(to state: ViewState) {
        DispatchQueue.main.async {
            self.viewState = state
            self.isLoading = state == .loading
        }
    }
}


enum ViewState {
    case empty
    case loading
    case ready
    case error
}

