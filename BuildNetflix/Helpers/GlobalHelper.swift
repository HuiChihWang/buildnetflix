//
//  GlobalHelper.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/11.
//

import Foundation
import SwiftUI

let exampleVideoURL = URL(string: "https://www.radiantmediaplayer.com/media/big-buck-bunny-360p.mp4")!
let exampleImageURL1 = URL(string: "https://picsum.photos/300/104")!
let exampleImageURL2 = URL(string: "https://picsum.photos/300/105")!
let exampleImageURL3 = URL(string: "https://picsum.photos/300/106")!

let exampleTrailer1 = Trailer(name: "Season 3 Trailer", videoURL: exampleVideoURL, thumbnailImageURL: exampleImageURL1)
let exampleTrailer2 = Trailer(name: "The Hero's Journey", videoURL: exampleVideoURL, thumbnailImageURL: exampleImageURL2)
let exampleTrailer3 = Trailer(name: "The Mysterious", videoURL: exampleVideoURL, thumbnailImageURL: exampleImageURL3)

let exampleTrailers = [exampleTrailer1, exampleTrailer2, exampleTrailer3].shuffled()

let exampleDetail1 = MovieDetail(year: 1991, rating: "Best", numberOfSeason: 3, resolution: "4K", promotionTitle: "Top 10 In Taiwan", creater: "Gilbert", cast: "Sally, Gilert, Eric")
let exampleDetail2 = MovieDetail(year: 2002, rating: "Good", numberOfSeason: 3, resolution: "HD", promotionTitle: "Watch Season 3 Now", creater: "Gilbert", cast: "Sally, Gilert, Eric")
let exampleDetail3 = MovieDetail(year: 2021, rating: "Recommeded", numberOfSeason: 1, resolution: "720P", creater: "Gilbert", cast: "Sally, Gilert, Eric")


let exampleEpisodeInfo11 = EpisodeInfo(title: "Days Gone Bye",season: 1, episode: 1, description: "TDeputy Sheriff Rick Grimes awakens from a coma, and searches for his family in a world ravaged by the undead.")

let exampleEpisodeInfo12 = EpisodeInfo(title: "Guts",season: 1, episode: 2, description: "In Atlanta, Rick is rescued by a group of survivors, but they soon find themselves trapped inside a department store surrounded by walkers.")

let exampleEpisodeInfo13 = EpisodeInfo(title: "Tell It to the Frogs",season: 1, episode: 3, description: "The group's plan to head for Fort Benning is put on hold when Sophia goes missing.")

let exampleEpisodeInfo21 = EpisodeInfo(title: "What Lies Ahead",season: 2, episode: 1, description: "The Professor recruits a young female robber and seven other criminals for a grand heist, targeting the Royal Mint of Spain.")

let exampleEpisodeInfo22 = EpisodeInfo(title: "Bloodletting",season: 2, episode: 2, description: "After Carl is accidentally shot, the group are brought to a family living on a nearby farm. Shane makes a dangerous trip in search of medical supplies.")

let exampleEpisodeInfo23 = EpisodeInfo(title: "Save the Last One",season: 2, episode: 3, description: "As Carl's condition continues to deteriorate, Shane and Otis try to dodge the walkers as they head back to the farm.")


let exampleEpisode11 = Episode(info: exampleEpisodeInfo11,
                             thumbnailImageURLString: "https://picsum.photos/300/104",
                             videoURL: exampleVideoURL)

let exampleEpisode12 = Episode(info: exampleEpisodeInfo12,
                             thumbnailImageURLString: "https://picsum.photos/300/105",
                             videoURL: exampleVideoURL)

let exampleEpisode13 = Episode(info: exampleEpisodeInfo13,
                             thumbnailImageURLString: "https://picsum.photos/300/106",
                             videoURL: exampleVideoURL)
let exampleEpisode21 = Episode(info: exampleEpisodeInfo21,
                             thumbnailImageURLString: "https://picsum.photos/300/107",
                             videoURL: exampleVideoURL)

let exampleEpisode22 = Episode(info: exampleEpisodeInfo22,
                             thumbnailImageURLString: "https://picsum.photos/300/108",
                             videoURL: exampleVideoURL)

let exampleEpisode23 = Episode(info: exampleEpisodeInfo23,
                             thumbnailImageURLString: "https://picsum.photos/300/109",
                             videoURL: exampleVideoURL)


let exampleEpisodes = [exampleEpisode11, exampleEpisode12, exampleEpisode13, exampleEpisode21, exampleEpisode22, exampleEpisode23]


var randomColor: Color {
    let colors: [Color] = [.red, .yellow, .blue, .green, .orange, .purple, .pink]
    return colors.randomElement()!
}

let exampleMovie1 = Movie(id: UUID().uuidString, name: "DARK", thumbnailURL: URL(string: "https://picsum.photos/200/300")!, detail: exampleDetail1, catogories: ["Dystopian", "Exciting", "Supenseful", "Sci-Fi TV"], moreLikeMovies: [exampleMovie2, exampleMovie3, exampleMovie4, exampleMovie5, exampleMovie6, exampleMovie7].shuffled(), trailers: exampleTrailers, defaultEpisode: exampleEpisodeInfo11, currentEpisode: exampleEpisodeInfo12, episodes: exampleEpisodes, previewImageName: "travelersPreview")

let exampleMovie2 = Movie(id: UUID().uuidString, name: "Travelers", thumbnailURL: URL(string: "https://picsum.photos/200/300/")!, detail: exampleDetail2, catogories: ["Dystopian", "Exciting", "Supenseful", "Sci-Fi TV"], moreLikeMovies: [], trailers: exampleTrailers, defaultEpisode: exampleEpisodeInfo11, currentEpisode: exampleEpisodeInfo12, previewImageName: "ozarkPreview")

let exampleMovie3 = Movie(id: UUID().uuidString, name: "Community", thumbnailURL: URL(string: "https://picsum.photos/200/301")!, detail: exampleDetail3, catogories: ["Dystopian", "Exciting", "Supenseful", "Sci-Fi TV"], moreLikeMovies: [], trailers: exampleTrailers, defaultEpisode: exampleEpisodeInfo11, currentEpisode: exampleEpisodeInfo12, previewImageName: "darkPreview")

let exampleMovie4 = Movie(id: UUID().uuidString, name: "Alone", thumbnailURL: URL(string: "https://picsum.photos/200/302")!, detail: exampleDetail1, catogories: ["Dystopian", "Exciting", "Supenseful", "Sci-Fi TV"], moreLikeMovies: [], trailers: exampleTrailers, defaultEpisode: exampleEpisodeInfo11, previewImageName: "travelersPreview")

let exampleMovie5 = Movie(id: UUID().uuidString, name: "Hannibal", thumbnailURL: URL(string: "https://picsum.photos/200/303")!, detail: exampleDetail1, catogories: ["Dystopian", "Exciting", "Supenseful", "Sci-Fi TV"], moreLikeMovies: [], trailers: exampleTrailers, defaultEpisode: exampleEpisodeInfo11, previewImageName: "dirtyJohnPreview")

let exampleMovie6 = Movie(id: UUID().uuidString, name: "After Line", thumbnailURL: URL(string: "https://picsum.photos/200/304")!, detail: exampleDetail1, catogories: ["Dystopian", "Exciting", "Supenseful", "Sci-Fi TV"], moreLikeMovies: [], trailers: exampleTrailers, defaultEpisode: exampleEpisodeInfo11, previewImageName: "whiteLinesPreview")

let exampleMovie7 = Movie(id: UUID().uuidString, name: "After Line", thumbnailURL: URL(string: "https://picsum.photos/200/304")!, detail: exampleDetail1, catogories: ["Dystopian", "Exciting", "Supenseful", "Sci-Fi TV"], moreLikeMovies: [], trailers: exampleTrailers, defaultEpisode: exampleEpisodeInfo11, previewImageName: "arrestedDevPreview")

var exampleMovies: [Movie] {
    [exampleMovie1, exampleMovie2, exampleMovie3, exampleMovie5, exampleMovie6, exampleMovie4, exampleMovie7].shuffled()
}



extension LinearGradient {
    static let blackOpacityGradient = LinearGradient(
        gradient: Gradient(colors: [Color.black.opacity(0), Color.black.opacity(0.95)]),
        startPoint: .top,
        endPoint: .bottom)
}

extension String {
    func widthOfString(using font: UIFont) -> CGFloat {
        let fontAttribute = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttribute)
        return size.width
    }
}

extension View {
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
