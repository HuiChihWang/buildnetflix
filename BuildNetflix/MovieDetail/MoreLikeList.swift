//
//  MoreLikeList.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/15.
//

import SwiftUI

struct MoreLikeList: View {
    var movies: [Movie]
    
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        LazyVGrid(columns: columns) {
            ForEach(movies) { movie in
                StandardHomeMovie(movie: movie)
            }
        }
        
    }
}

struct MoreLikeList_Previews: PreviewProvider {
    static var previews: some View {
        MoreLikeList(movies: exampleMovies)
    }
}
