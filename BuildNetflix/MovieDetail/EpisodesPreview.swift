//
//  EpisodesPreview.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/16.
//

import SwiftUI

struct EpisodesPreview: View {
    var episodes: [Episode]
    
    @Binding var isShowSeasonPicker: Bool
    @Binding var selectedSeason: Int
    
    private func getEpisodes(from season: Int) -> [Episode] {
        return episodes.filter { $0.info.season == season }
    }
    
    var body: some View {
        VStack(spacing:14) {
            // season picker
            HStack {
                Button(action: {
                    isShowSeasonPicker = true
                }, label: {
                    Group {
                        Text("Season \(selectedSeason)")
                        Image(systemName: "chevron.down")
                    }
                    .font(.system(size: 16))
                    
                })
                Spacer()
            }
            
            // episodes list
            ForEach(getEpisodes(from: selectedSeason)) { episode in
                VStack(alignment: .leading) {
                    HStack {
                        VideoPreviewImage(imageURL: episode.thumnailImageURL, videoURL: episode.videoURL)
                            .frame(width: 120, height: 70)
                            .clipped()
                            .padding(.trailing, 3)

                        VStack(alignment: .leading) {
                            Text("\(episode.info.episode). \(episode.info.title)")
                                .font(.system(size: 16))
                            Text("\(episode.info.durationInMinute)m")
                                .font(.system(size: 12))
                                .foregroundColor(.gray)
                        }
                        
                        Spacer()

                        Image(systemName: "arrow.down.to.line.alt")
                            .font(.system(size: 20))
                    }
                    Text(episode.info.description)
                        .font(.system(size: 13))
                        .lineLimit(3)
                }
                .padding(.bottom, 20)
            }
            
            Spacer()
        }
        .foregroundColor(.white)
        .padding(.horizontal, 20)
    }
}

struct EpisodesPreview_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            EpisodesPreview(episodes: exampleEpisodes, isShowSeasonPicker: .constant(false), selectedSeason: .constant(1))
        }
    }
}
