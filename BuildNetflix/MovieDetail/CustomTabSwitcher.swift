//
//  CustomTabSwitcher.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/14.
//

import SwiftUI

struct CustomTabSwitcher: View {
    var tabs: [CustomTab]
    var fontSize: CGFloat = 16
    var fontWeight = Font.Weight.bold
    var uiFontWeight = UIFont.Weight.bold
    
    var chosenMovie: Movie
    
    @State private var currentTab: CustomTab = .episodes
    @Binding var showSeasonPicker: Bool
    @Binding var selectedSeason: Int
    
    func widthForTab(_ tab: CustomTab) -> CGFloat {
        let string = tab.rawValue
        return string.widthOfString(using: .systemFont(ofSize: fontSize, weight: uiFontWeight))
    }
    
    var body: some View {
        VStack {
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 20) {
                    ForEach(tabs, id: \.self) { tab in
                        VStack {
                            Rectangle()
                                .cornerRadius(3.0)
                                .frame(width: widthForTab(tab), height: 6)
                                .foregroundColor(tab == currentTab ? .red : .clear)
                            
                        
                            Button(action: {
                                currentTab = tab
                            }, label: {
                                Text(tab.rawValue)
                                    .font(.system(size: fontSize, weight: fontWeight))
                            })
                            .frame(width: widthForTab(tab), height: 60)
                        }
                    }
                }
            }
            .padding(.horizontal, 10)
            
            switch currentTab {
            case .episodes:
                EpisodesPreview(episodes: chosenMovie.episodes ?? [], isShowSeasonPicker: $showSeasonPicker, selectedSeason: $selectedSeason)
            case .trailers:
                TrailerList(trailers: chosenMovie.trailers)
            case .more:
                MoreLikeList(movies: chosenMovie.moreLikeMovies)
            }
        }
        .foregroundColor(.white)
    }
}

enum CustomTab: String {
    case episodes = "EPISODES"
    case trailers = "TRAILER & MORE"
    case more = "MORE LIKE THIS"
}

struct CustomTabSwitcher_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black.ignoresSafeArea(.all)
            CustomTabSwitcher(tabs: [.episodes, .trailers, .more], chosenMovie: exampleMovie1, showSeasonPicker: .constant(false), selectedSeason: .constant(1))
        }
        
    }
}
