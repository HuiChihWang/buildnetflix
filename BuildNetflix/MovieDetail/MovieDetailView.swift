//
//  MovieDetail.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/13.
//

import SwiftUI

struct MovieDetailView: View {
    var movie: Movie
    static let screenSize = UIScreen.main.bounds
    
    @State private var isShowSeasonPicker = false
    @State private var selectedSeason = 1
    
    @Binding var movieDetailToShow: Movie?
    
    var pickerOptions: [String] {
        var options: [String] = []
        let numberSeason = movie.detail.numberOfSeason ?? 0
        for season in 0..<numberSeason {
            options.append("Season \(season + 1)")
        }
        return options
    }
    @ViewBuilder
    private func selectorView() -> some View {
            Group {
                Color.black.opacity(0.9)
                VStack(spacing: 40) {

                    Spacer()
                    ForEach( 0 ..< (movie.detail.numberOfSeason ?? 0)) { season in
                        Button(action: {
                            self.selectedSeason = season + 1
                            self.isShowSeasonPicker = false
                        }, label: {
                            Text("Season \(season + 1)")
                                .foregroundColor(isSelectedSeason(for: season + 1) ? .white : .gray)
                                .font(.system(size: isSelectedSeason(for: season + 1) ? 25 : 20, weight: .bold))
                        })

                    }
                    Spacer()

                    Button(action: {
                        self.isShowSeasonPicker = false
                    }, label: {
                        Image(systemName: "x.circle.fill")
                            .foregroundColor(.white)
                            .font(.system(size: 40))
                            .scaleEffect(1.1)
                    })
                    .padding(.bottom, 30)
                }
            }
            .edgesIgnoringSafeArea(.all)
    }
    
    @ViewBuilder
    private func mainView() -> some View {
        VStack {
            HStack {
                Spacer()
                Button(action: {
                    movieDetailToShow = nil
                }, label: {
                    ZStack {
                        Circle()
                            .foregroundColor(Color(#colorLiteral(red: 0.370555222, green: 0.3705646992, blue: 0.3705595732, alpha: 1)))
                            .frame(width: 35, height: 35)
                        Image(systemName: "xmark")
                            .font(.system(size: 20))
                    }
                })
                .padding(.horizontal, 24)
                .padding(.top, 10)
            }
            
            ScrollView(/*@START_MENU_TOKEN@*/.vertical/*@END_MENU_TOKEN@*/, showsIndicators: false) {
                VStack {
                    StandardHomeMovie(movie: movie)
                        .frame(width: MovieDetailView.screenSize.width * 0.4,
                               height: MovieDetailView.screenSize.height * 0.3)
                    
                    MovieInfoSubheadline(movie: movie)
                        .padding(.vertical, 7)
                    
                    Text(movie.detail.promotionTitle ?? "")
                        .font(.headline)
                        .bold()
                    
                    RectangleButton(text: "Play", imageName: "play.fill", backgroundColor: .red, forgroundColor: .white) {
                        // TODO: action
                    }
                    
                    
                    CurrentEpisodeInfoView(movie: movie)
                    
                    
                    CastInfoView(movie: movie)
                        .padding(.vertical, 5)
                    
                    HStack(spacing: 16) {
                        SmallVerticalButton(text: "My List", isOffImage: "plus", isOnImage: "checkmark", isOn: true) {
                            
                        }
                        
                        SmallVerticalButton(text: "Rate", isOffImage: "hand.thumbsup", isOnImage: "hand.thumbsup.fill", isOn: true) {
                            
                        }
                        SmallVerticalButton(text: "Share", isOffImage: "square.and.arrow.up", isOnImage: "square.and.arrow.up", isOn: true) {
                            
                        }
                        
                        Spacer()
                    }
                    .padding(.leading, 20)
                    
                    
                    
                }
                .padding(.horizontal, 10)
                
                CustomTabSwitcher(tabs: [.episodes, .trailers, .more], chosenMovie: movie, showSeasonPicker: $isShowSeasonPicker, selectedSeason: $selectedSeason)
            }
            
            Spacer()
        }
    }
    
    private func isSelectedSeason(for season: Int) -> Bool{
        return selectedSeason == season
    }
    
    var body: some View {
        ZStack {
            Color.black
                .ignoresSafeArea(.all)
            
            ZStack {
                mainView()
                    .foregroundColor(.white)
                
                if isShowSeasonPicker {
                    selectorView()
                }
            }
        }
    }
}


struct CurrentEpisodeInfoView: View {
    var movie: Movie
    
    var episodeTitleDisplay: String {
        let episode = movie.currentEpisode ?? movie.defaultEpisode
        return "S\(episode.season):E\(episode.episode) \(episode.title)"
    }
    
    var body: some View {
        // see alignment guide
        VStack {
            HStack {
                Text(episodeTitleDisplay)
                    .bold()
                Spacer()
            }
            .padding(.vertical, 4)
            
            HStack {
                Text(movie.description)
                    .font(.subheadline)
                Spacer()
            }
        }
    }
}

struct CastInfoView: View {
    var movie: Movie
    
    var body: some View {
        VStack(spacing: 3) {
            HStack {
                Text("Cast: \(movie.detail.cast)")
                Spacer()
            }
            
            HStack {
                Text("Creator: \(movie.detail.creater)")
                Spacer()
            }
        }
        .font(.caption)
        .foregroundColor(.gray)
    }
}

struct MovieDetail_Previews: PreviewProvider {
    static var previews: some View {
        let movie = generateMovies(1)
        return MovieDetailView(movie: movie[0], movieDetailToShow: .constant(nil))
    }
}
