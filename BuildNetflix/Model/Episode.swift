//
//  Episode.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/13.
//

import Foundation

struct Episode: Identifiable {
    var info: EpisodeInfo
    var id = UUID().uuidString
    var thumbnailImageURLString: String
    var videoURL: URL
    var thumnailImageURL: URL {
        URL(string: thumbnailImageURLString)!
    }
}

struct EpisodeInfo: Hashable, Equatable {
    var title: String
    var season: Int
    var episode: Int
    var description: String
    var durationInMinute: Int = 0
}
