//
//  Movie.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/11.
//

import Foundation

struct Movie: Identifiable {
    var id: String
    var name: String
    var thumbnailURL: URL
    var detail: MovieDetail
    var catogories: [String]
    var moreLikeMovies: [Movie]
    var trailers: [Trailer]
    
    
    var defaultEpisode: EpisodeInfo
    var currentEpisode: EpisodeInfo?
    var episodes: [Episode]?
    
    var movieType: MovieType {
        episodes != nil ? .tvShow : .movie
    }
    
    var movieGenre: HomeGenre = .AllGenres
    
    var description: String {
        currentEpisode != nil ? currentEpisode!.description : defaultEpisode.description
    }
    
    var previewImageName: String
    var previewVideoURL: URL? = exampleVideoURL
}

enum MovieType {
    case movie
    case tvShow
}

struct MovieDetail {
    var year: Int
    var rating: String
    var numberOfSeason: Int?
    var resolution: String
    var promotionTitle: String?
    var creater: String
    var cast: String
}


