//
//  Trailer.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/16.
//

import Foundation

struct Trailer: Identifiable, Hashable {
    var id: String = UUID().uuidString
    var name: String
    var videoURL: URL
    var thumbnailImageURL: URL
}
