//
//  ComingSoonVM.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/20.
//

import Foundation

class ComingSoonVM:  ObservableObject {
    var movies: [Movie] = []
    
    init() {
        movies = generateMovies(20)
    }
}
