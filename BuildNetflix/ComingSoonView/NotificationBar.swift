//
//  NotificationBar.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/19.
//

import SwiftUI

struct NotificationBar: View {
    @Binding var showNotification: Bool
    
    var body: some View {
        Button(action: {
            showNotification = true
        }, label: {
            HStack {
                Image(systemName: "bell")
                Text("Notification")
                    .bold()
                Spacer()
                Image(systemName: "chevron.right")
            }
            .font(.system(size: 18, weight: .bold))
        })
        .foregroundColor(.white)
    }
}

struct NotificationBar_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            NotificationBar(showNotification: .constant(false))
        }
    }
}
