//
//  DownloadView.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/20.
//

import SwiftUI

struct DownloadView: View {
    @State private var isSmartDownloadEnable: Bool = false
    
    var body: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            
            VStack {
                HStack {
                    Image(systemName: "info.circle")
                    Text("Smart Download")
                    Text(isSmartDownloadEnable ? "ON" : "OFF")
                        .foregroundColor(.blue)
                    Spacer()
                }
                .padding(.vertical, 10)
                
                Spacer()
                
                VStack {
                    DownloadLogo()
                        .frame(width: 150, height: 150)
                        .padding(.bottom, 20)
                    
                    VStack {
                        Text("Never be without Netflix")
                            .font(.title)
                            .bold()
                            .padding(.bottom, 10)
                            
                        Text("Download shows and movies so you'll never be without something to watch -- even when you are offline.")
                            .multilineTextAlignment(.center)
                            .lineLimit(3)
                            
                    }
                    .padding(.horizontal, 30)
                    .padding(.bottom, 20)
                    
                    
                    Button(action: {}, label: {
                        Text("See What You Can Download")
                            .bold()
                            .font(.system(size: 16))
                            .foregroundColor(.black)
                            .padding(10)
                            .background(Color.white)
                    })
                }
                Spacer()
            }
            .padding(.horizontal, 10)
                
            }
            .foregroundColor(.white)
            

    }
}

struct DownloadView_Previews: PreviewProvider {
    static var previews: some View {
        DownloadView()
    }
}

struct DownloadLogo: View {
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Circle()
                    .foregroundColor(.graySearchBackground)
                    .frame(width: geometry.size.width, height: geometry.size.width, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                
                Image(systemName: "arrow.down.to.line.alt")
                    .resizable()
                    .scaledToFit()
                    .foregroundColor(.graySearchText)
                    .frame(width: geometry.size.width * 0.5)
                
            }
        }
    }
}
