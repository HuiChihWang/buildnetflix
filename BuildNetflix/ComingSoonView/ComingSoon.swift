//
//  ComingSoon.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/19.
//

import SwiftUI

struct ComingSoon: View {
    @State private var showNotificationBar: Bool = false
    @State private var navBarHidden = true
    
    @ObservedObject private var vm = ComingSoonVM()
    @State private var movieDetailToShow: Movie?
    
    @State private var scrollOffSet: CGFloat = 0.0
    @State private var activeIndex: Int = 0
    
    let cellHeight: CGFloat = 400
    let notibarHeight: CGFloat = 70
    
    private func updateActiveIndex(from scrollPos: CGFloat) {
        let activeIndexDouble = Double((scrollPos - notibarHeight) / (cellHeight + 5))
        activeIndex = Int(ceil(activeIndexDouble))
    }
    
    var body: some View {
        
        // check state change by binding here, use another method to refactor
        // https://medium.com/better-programming/three-ways-to-react-to-state-changes-in-swiftui-a30545c72361
        
        
        let scrollBinding = Binding {
            return scrollOffSet
        }
        set: { newVal in
            scrollOffSet = newVal
            updateActiveIndex(from: newVal)
        }
        
        return VStack {
            ZStack {
                Color.black
                    .edgesIgnoringSafeArea(.all)
                
                TrackableScrollView(.vertical, showIndicators: false, contentOffset: scrollBinding) {
                    LazyVStack {
                        NotificationBar(showNotification: $showNotificationBar)
                            .frame(height: notibarHeight)
                        
                        ForEach(vm.movies.indices) { movieIndex in
                            ComingSoonRow(movie: vm.movies[movieIndex], movieDetailToShow: $movieDetailToShow)
                                .frame(height: cellHeight)
                                .opacity(movieIndex == activeIndex ? 1.0 : 0.4)
                                .animation(.easeInOut)
                        }
                    }
                }
                .foregroundColor(.white)
                
                
                //                VStack {
                //                    Text("offset: \(scrollOffSet)")
                //                    Text("activeIndex: \(activeIndex)")
                //                }
                //                .background(Color.red)
                //                .font(.title)
            }
            
            //            // what's this ?
            NavigationLink(
                destination: Text("Notification List"),
                isActive: $showNotificationBar,
                label: {
                    EmptyView()
                })
                .navigationTitle("")
                .navigationBarHidden(navBarHidden)
                .onReceive(NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification), perform: { _ in
                    self.navBarHidden = true
                })
                .onReceive(NotificationCenter.default.publisher(for: UIApplication.willResignActiveNotification), perform: { _ in
                    self.navBarHidden = false
                })
        }
    }
}

struct ComingSoon_Previews: PreviewProvider {
    static var previews: some View {
        ComingSoon()
    }
}
