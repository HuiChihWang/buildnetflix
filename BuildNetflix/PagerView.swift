//
//  PagerView.swift
//  BuildNetflix
//
//  Created by Hui Chih Wang on 2020/11/20.
//

import SwiftUI

struct PagerView<Content: View>: View {
    let pageCount: Int
    let content: Content
    
    @Binding var currentIndex: Int
    @Binding var translation: CGFloat
    
    init(pageCount: Int,
         currentIndex: Binding<Int>,
         translation: Binding<CGFloat>,
         @ViewBuilder content: () -> Content
    ) {
        self.pageCount = pageCount
        self._currentIndex = currentIndex
        self.content = content()
        self._translation = translation
    }
    
    var body: some View {
        GeometryReader { geometry in
            HStack(spacing: 0) {
                self.content
                    .frame(width: geometry.size.width)
            }
            .frame(width: geometry.size.width, alignment: .leading)
            .offset(x: -CGFloat(self.currentIndex) * geometry.size.width)
            .offset(x: self.translation)
            .animation(.interactiveSpring())
            .gesture(
                DragGesture()
                    .onChanged({ (value) in
                        translation = value.translation.width
                    })
                    .onEnded({ (value) in
                        let offset = value.translation.width / geometry.size.width
                        let newIndex = (CGFloat(self.currentIndex) - offset).rounded()
                        var newIndexInt = Int(newIndex)
                        newIndexInt = min(pageCount - 1, newIndexInt)
                        newIndexInt = max(0, newIndexInt)
                        currentIndex = newIndexInt
                        translation = 0
                    })
            )
            
        }
    }
}

struct PagerDummy: View {
    @State private var currentIndex: Int = 0
    @State private var translation: CGFloat = .zero
    
    var body: some View {
        ZStack {
            PagerView(pageCount: 3, currentIndex: $currentIndex, translation: $translation) {
                Color.red
                Color.blue
                Color.orange
            }
            
            Text("translation: \(translation)")
                .background(Color.black)
                .foregroundColor(.white)
        }

    }
}

struct PagerView_Previews: PreviewProvider {
    static var previews: some View {
        PagerDummy()
    }
}
